<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/online/([\\.\\-0-9a-zA-Z]+)(/?)([^/]*)#",
		"RULE" => "alias=\$1",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/about_company/nashi-klienty/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about_company/nashi-klienty/index.php",
	),
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/site2personal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/site2personal/order/index.php",
	),
	array(
		"CONDITION" => "#^/online/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/stssync/calendar/#",
		"RULE" => "",
		"ID" => "bitrix:stssync.server",
		"PATH" => "/bitrix/services/stssync/calendar/index.php",
	),
	array(
		"CONDITION" => "#^/personal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/personal/order/index.php",
	),
	array(
		"CONDITION" => "#^/site2personal/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.section",
		"PATH" => "/site2personal/index.php",
	),
	array(
		"CONDITION" => "#^/site2catalog/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/site2catalog/index.php",
	),
	array(
		"CONDITION" => "#^/our-services/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/our-services/index.php",
	),
	array(
		"CONDITION" => "#^/our-services/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/services/index.php",
	),
	array(
		"CONDITION" => "#^/site2store/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => "/site2store/index.php",
	),
	array(
		"CONDITION" => "#^/site2news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/site2news/index.php",
	),
	array(
		"CONDITION" => "#^/catalog/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/store/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => "/store/index.php",
	),
);

?>