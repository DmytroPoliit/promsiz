<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("how-to-buy");
?><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("how-to-buy");
?><section class="text-page">
    <div class="container">

        <img src="<?=SITE_TEMPLATE_PATH?>/img/jpeg_files/serv-5.jpg" alt="image" align="left">


        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/include/how-to-buy_dynamic_field/about.php"
            )
        );?>

        <h4><?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/include/how-to-buy_dynamic_field/Headline_numbered_li.php"
            )
        );?></h4>


        <ol>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/how-to-buy_dynamic_field/numbered_li.php"
                )
            );?>
        </ol>

        <h4><?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/how-to-buy_dynamic_field/Headline_unnumbered_li.php"
                )
            );?></h4>

        <ul>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/how-to-buy_dynamic_field/unnumbered_li.php"
                )
            );?>
        </ul>

        <h4>Цитата</h4>

        <blockquote class="blockquote">
            <i class="icon-sprite icon-blockquote-label"></i>

            <div class="blockquote-img">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/jpeg_files/blockquote-img.jpg" alt="foto">
            </div>
            <div class="blockquote-text">
                <p>
                    <i>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => "/include/how-to-buy_dynamic_field/qoute_maintext.php"
                            )
                        );?>
                    </i>
                </p>
                <p>
                        <b>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/include/how-to-buy_dynamic_field/quote_name.php"
                                )
                            );?>
                            ,
                        </b>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/include/how-to-buy_dynamic_field/qoute_position.php"
                        )
                    );?>
                </p>

            </div>

        </blockquote>

        <h4>Таблица</h4>

        <table>
            <tbody>
            <tr>
                <td>№</td>
                <td>Название товара</td>
                <td>Количество</td>
                <td>Цена</td>
            </tr>
            <tr>
                <td>1</td>
                <td>Перчатки антивибрационные Mechanix AV</td>
                <td>3шт.</td>
                <td>768грн.</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Костюм МАСТЕР-ЦЕХ /46-Гр/ /new/</td>
                <td>5шт.</td>
                <td>1 569 грн.</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Пояс предохранительный - 4 ПБ</td>
                <td>1шт.</td>
                <td>460 грн.</td>
            </tr>
            <tr>
                <td>4</td>
                <td>Очки защитные открытые c прозрачными линзами STARLINE</td>
                <td>10шт.</td>
                <td>90 грн.</td>
            </tr>
            <tr>
                <td>5</td>
                <td>Перчатки антивибрационные Mechanix AV</td>
                <td>32шт.</td>
                <td>245 грн.</td>
            </tr>

            </tbody>
        </table>

    <h4>Видео с канала YouTube</h4>

    <iframe width="100%" height="480" src="https://www.youtube.com/embed/CBDbxZY7hng?rel=0?ecver=1" frameborder="0" allowfullscreen></iframe>

    </div>
</section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>