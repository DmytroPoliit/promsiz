<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("news");
?>

<?
echo "<pre>";
echo print_r($arResult);
echo "</pre>";?>
    <section class="block-bottom-padding--md">
        <div class="container">
            <div class="sidebar">

                <p class="sidebar__title bold-text">Категории товаров</p>
                <ul class="sidebar__list">
                    <li><a href="#">Все товары на распродаже</a></li>
                    <li><a href="#">Спецобувь</a></li>
                    <li><a href="#">Рабочие перчатки</a></li>
                    <li><a href="#">Средства защиты глаз и лица</a></li>
                    <li><a href="#">Средства защиты головы</a></li>
                    <li><a href="#">Средства защиты органов слуха</a></li>
                    <li><a href="#">Средства защиты органов дыхания</a></li>
                    <li><a href="#">ДСИЗ</a></li>
                    <li><a href="#">Защита от электрического тока</a></li>
                    <li><a href="#">Защитные системы блокировки LOTO</a></li>
                    <li><a href="#">Средства защиты от падения с высоты</a></li>
                    <li><a href="#">Другое</a></li>
                    <li><a href="#">Распродажа</a></li>
                </ul>

                <?php include_once "parts/filter.php" ?>

            </div>
            <div class="right-block">

                <a class="catalog-title-mob bold-text">Рабочие перчатки</a>


                <h2 class="right-block__title">Все товары “Распродажа”</h2>

                <div class="goods-sort-panel">
                    <div class="goods-sort__select">
                        <label for="sel-1">Сортировать:</label>
                        <select name="sort" id="sel-1" class="selectpicker">
                            <option value="1">по популярности</option>
                            <option value="2">по возрастанию</option>
                            <option value="3">по убыванию</option>
                            <option value="4">some action</option>
                            <option value="5">some action</option>
                            <option value="6">some action</option>
                        </select>
                    </div>
                    <div class="goods-sort__kind">
                        <a href="#" class="goods-sort__kind--col active">
                            <span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
                        </a>
                        <a href="/promSIZ/08.catalog-2lvl-row.php" class="goods-sort__kind--row">
                            <span></span><span></span><span></span>
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>

                <ul class="remedies-list">
                    <?/*
                    if (CModule::IncludeModule("price"))

                    */?>
                    <?
                    if (CModule::IncludeModule("catalog.section")){
                    $dbProductDiscounts = CCatalogDiscount::GetList(
                        array("SORT" => "ASC"),
                        array("ACTIVE" => "Y"),
                        false,
                        false,
                        array("ID", "PRODUCT_ID")
                    );
                    while ($arProductDiscounts = $dbProductDiscounts->Fetch()) {
                        $akcii_arr[] = $arProductDiscounts["PRODUCT_ID"];
                    }
                    foreach($akcii_arr as $ID) {
                        $akcii_arr_[] = $ID;
                    }
                    $GLOBALS['arrFilterAkcii'] = array("ID"=>$akcii_arr_);
                    ?>
                    <?$APPLICATION->IncludeComponent("bitrix:catalog.top", "hit_on_main", Array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "IBLOCK_TYPE" => "1c_catalog",	// Тип инфоблока
                        "IBLOCK_ID" => "12",	// Инфоблок
                        "ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
                        "ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
                        "ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
                        "ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
                        "FILTER_NAME" => "arrFilterAkcii",	// Имя массива со значениями фильтра для фильтрации элементов
                        "HIDE_NOT_AVAILABLE" => "Y",	// Товары, которых нет на складах
                        "ELEMENT_COUNT" => "10",	// Количество выводимых элементов
                        "LINE_ELEMENT_COUNT" => "1",	// Количество элементов выводимых в одной строке таблицы
                        "PROPERTY_CODE" => array(	// Свойства
                            0 => "",
                            1 => "",
                        ),
                        "OFFERS_FIELD_CODE" => array(	// Поля предложений
                            0 => "",
                            1 => "",
                        ),
                        "OFFERS_PROPERTY_CODE" => array(	// Свойства предложений
                            0 => "",
                            1 => "",
                        ),
                        "OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
                        "OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
                        "OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
                        "OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
                        "OFFERS_LIMIT" => "5",	// Максимальное количество предложений для показа (0 - все)
                        "VIEW_MODE" => "SECTION",	// Показ элементов
                        "TEMPLATE_THEME" => "blue",	// Цветовая тема
                        "PRODUCT_DISPLAY_MODE" => "N",	// Схема отображения
                        "ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
                        "LABEL_PROP" => "-",	// Свойство меток товара
                        "SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
                        "SHOW_OLD_PRICE" => "N",	// Показывать старую цену
                        "SHOW_CLOSE_POPUP" => "N",	// Показывать кнопку продолжения покупок во всплывающих окнах
                        "MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
                        "MESS_BTN_COMPARE" => "Сравнить",	// Текст кнопки "Сравнить"
                        "MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
                        "SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
                        "DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
                        "SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
                        "PRODUCT_QUANTITY_VARIABLE" => "",	// Название переменной, в которой передается количество товара
                        "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                        "SEF_RULE" => "",	// Правило для обработки
                        "CACHE_TYPE" => "N",	// Тип кеширования
                        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                        "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                        "ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
                        "PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
                        "PRICE_CODE" => array(	// Тип цены
                            0 => "BASE",
                        ),
                        "USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
                        "SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
                        "PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
                        "CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
                        "BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
                        "USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
                        "ADD_PROPERTIES_TO_BASKET" => "Y",	// Добавлять в корзину свойства товаров и предложений
                        "PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
                        "PRODUCT_PROPERTIES" => "",	// Характеристики товара
                        "OFFERS_CART_PROPERTIES" => "",	// Свойства предложений, добавляемые в корзину
                        "ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
                        "DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
                        "ROTATE_TIMER" => "30"
                    ),
                        false
                    );}?>
                </ul>


            </div>
        </div>
    </section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>