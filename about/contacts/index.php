<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Задайте вопрос");
?><div class="container">
    <div class="col-md-7 col-sm-6 col-xs-12 no-padding">

        <ul class="contacts-list">

            <li class="contacts-list__item">
                <div class="contacts-list__item--image">
                    <object data="<?=SITE_TEMPLATE_PATH?>/img/svg-image/c-1.svg" type="image/svg+xml"></object>
                </div>
                <p class="contacts-list__item--title bold-text">Адрес</p>
                <ul>
                    <li>08298</li>
                    <li>Киевcкая область, Киево-Святошинский район, пгт. Коцюбинское,ул. Пономарева, 32</li>
                </ul>
            </li>

            <li class="contacts-list__item">
                <div class="contacts-list__item--image">
                    <object data="<?=SITE_TEMPLATE_PATH?>/img/svg-image/c-2.svg" type="image/svg+xml"></object>
                </div>
                <p class="contacts-list__item--title bold-text">Телефоны</p>
                <ul>
                    <li><a href="tel:+38 (044) 390-40-90">+38 (044) 390-40-90 (многоканальный) </a></li>
                    <li><a href="tel:+38 (044) 390-40-72">+38 (044) 390-40-72 (многоканальный) </a></li>
                    <li><a href="tel:+38 (044) 503-70-70 ">+38 (044) 503-70-70 </a></li>
                    <li><a href="tel:+38 (044) 503-70-77">+38 (044) 503-70-77</a></li>
                </ul>
            </li>

            <li class="contacts-list__item">
                <div class="contacts-list__item--image">
                    <object data="<?=SITE_TEMPLATE_PATH?>/img/svg-image/c-3.svg" type="image/svg+xml"></object>
                </div>
                <p class="contacts-list__item--title bold-text">График работы офиса</p>
                <ul>
                    <li>Пн. - Пт.: с 09:00 до 18:00 </li>
                    <li>Сб. - Вн.: выходной </li>
                </ul>
            </li>
            
            <li class="contacts-list__item">
                <div class="contacts-list__item--image">
                    <object data="<?=SITE_TEMPLATE_PATH?>/img/svg-image/c-4.svg" type="image/svg+xml"></object>
                </div>
                <p class="contacts-list__item--title bold-text">E-mail</p>
                <ul>
                    <li><a href="mailTo:info@promsiz.com.ua" class="link-accent">info@promsiz.com.ua</a></li>
                </ul>
            </li>

        </ul>

    </div>

    <?$APPLICATION->IncludeComponent(
	"bitrix:main.feedback",
	"feedback_form",
	Array(
		"COMPONENT_TEMPLATE" => "feedback_form",
		"EMAIL_TO" => "your.email@domain.com",
		"EVENT_MESSAGE_ID" => array(0=>"7",),
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"REQUIRED_FIELDS" => array(0=>"NAME",1=>"EMAIL",2=>"MESSAGE",),
		"USE_CAPTCHA" => "Y"
	)
);?>

</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	"map_contacts",
	Array(
		"COMPONENT_TEMPLATE" => "map_contacts",
		"CONTROLS" => array(0=>"SMALLZOOM",1=>"MINIMAP",2=>"TYPECONTROL",),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:50.44035264534188;s:10:\"yandex_lon\";d:30.598050190695936;s:12:\"yandex_scale\";i:14;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:30.59855299997319;s:3:\"LAT\";d:50.44291152280588;s:4:\"TEXT\";s:0:\"\";}}}",
		"MAP_HEIGHT" => "500",
		"MAP_ID" => "",
		"MAP_WIDTH" => "1600",
		"OPTIONS" => array(0=>"ENABLE_SCROLL_ZOOM",1=>"ENABLE_DRAGGING",)
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>