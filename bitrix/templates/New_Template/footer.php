<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-sm-6">
                        <h4 class="site-footer__title">Каталог товаров</h4>


                        <?$APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "f",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "left",
                                "COMPONENT_TEMPLATE" => ".default",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => "",
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "first_footer_menu",
                                "USE_EXT" => "N"
                            )
                        );?>
                    </div>
                    <div class="col-sm-6">
                        <h4 class="site-footer__title">Наша компания</h4>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "footer_menu_second",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "left",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(0=>"",),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "second_footer_menu",
                                "USE_EXT" => "N"
                            )
                        );?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="site-footer__right-block">
                    <h4 class="site-footer__title">Наша компания</h4>
                    <div class="site-footer__inform-block">
                        <ul class="site-footer__tel-block">
                            <li>
                                <a href="tel:+38 (044) 390-40-90">+38 (044) <span class="bold-text">390-40-90</span></a>
                            </li>
                            <li>
                                <a href="tel:+38 (044) 390-40-72">+38 (044) <span class="bold-text">390-40-72</span></a>
                            </li>
                        </ul>
                        <div class="site-footer__address-block footer-text">
                            <span>Адрес:</span>
                            <address>Киевская область, Киево-Святошинский район, пгт. Коцюбинское, ул. Пономарева, 32
                            </address>
                        </div>
                        <div class="site-footer__mail-block footer-text">
                            <span>E-mail:</span><a href="mailto:info@promsiz.com.ua">info@promsiz.com.ua</a>
                        </div>
                    </div>
                    <div class="site-footer__develop-block">
                        <span class="footer-span">Разработано:</span>
                        <a href="#"><i class="icon-sprite icon-dev-logo"></i></a>
                    </div>
                    <div class="site-footer__copyright-block">
                        <span class="footer-span"> &copy; 1998-2017, ПромСИЗ. Все права защищены</span>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" id="scroller"></a>
    </div>
</footer>

<!--///////////////////////////////////////////////// modals //////////////////////////////////////////////-->
<!-- Modal callback-1-->
<div id="modal-callback-1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <a class="close" data-dismiss="modal"><i class="icon-sprite icon-close"></i></a>
                <h4 class="modal-title">Получить помощь</h4>
            </div>
            <div class="modal-body">
                <form action="#">
                    <div class="modal-input-block">
                        <fieldset>
                            <input type="text" id="m-1" class="modal-input" placeholder="Введите ваше имя">
                            <label for="m-1" class="modal-input-label">Ваше имя</label>
                        </fieldset>
                        <fieldset>
                            <input type="tel" id="m-2" class="modal-input tel-input" placeholder="+38 (___) ___-__-__">
                            <label for="m-2" class="modal-input-label">Номер телефона</label>
                        </fieldset>
                    </div>

                    <button type="submit" class="btn btn-md btn--accent">Запросить обратный звонок</button>

                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal callback-2-->
<div id="modal-callback-2" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <a class="close" data-dismiss="modal"><i class="icon-sprite icon-close"></i></a>
                <h4 class="modal-title">Заказать звонок</h4>
            </div>
            <div class="modal-body">
                <form action="#">
                    <div class="modal-input-block">
                        <fieldset>
                            <input type="tel" id="m-3" class="modal-input tel-input error-input" placeholder="+38 (___) ___-__-__">
                            <label for="m-3" class="modal-input-label error-label">Номер телефона</label>
                        </fieldset>
                    </div>

                    <button type="submit" class="btn btn-md btn--accent">Отправить</button>

                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal order-->
<div id="modal-order" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <a class="close" data-dismiss="modal"><i class="icon-sprite icon-close"></i></a>
                <h4 class="modal-title">Заказать товар</h4>
            </div>
            <div class="modal-body">
                <form action="#">
                    <div class="modal-input-block">
                        <fieldset>
                            <input type="text" id="m-4" class="modal-input" placeholder="Введите ваше имя">
                            <label for="m-4" class="modal-input-label">Ваше имя</label>
                        </fieldset>
                        <fieldset>
                            <input type="tel" id="m-5" class="modal-input tel-input" placeholder="+38 (___) ___-__-__">
                            <label for="m-5" class="modal-input-label">Номер телефона</label>
                        </fieldset>
                        <fieldset>
                            <input type="text" id="m-6" class="modal-input" placeholder="Введите ваш e-mail">
                            <label for="m-6" class="modal-input-label">E-mail</label>
                        </fieldset>
                    </div>

                    <button type="submit" class="btn btn-md btn--accent">Заказать</button>

                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?=SITE_TEMPLATE_PATH?>/js/libs.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/common.js"></script>

</body>