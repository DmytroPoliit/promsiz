(function(Webvision) {
    window.Webvision = Webvision = Webvision || {};

    Webvision.Order = {
        selectors: {
            formWrapper: '#orderData',
            delivery: 'delivery-4'
        },

        init: function () {


            $('#u-7').mask('+38 (999) 999-99-99');

            $("input[type=radio][id=delivery-1]").on("change",function(){


                $( "#vertical-radio-item-C-O-D" ).attr('class', 'vertical-radio__item');




                if($( "#delivery-inputs-delivery-to-home" ).is(":visible")){



                }else{
                    $( "#delivery-inputs-delivery-to-home" ).show();
                    $( "#delivery-inputs-delivery-to-department" ).hide();
                    $( "#delivery-inputs-delivery-to-Kiev" ).hide();
                    $( "#delivery-inputs-pickup" ).hide();


                }

            });
            $("input[type=radio][id=delivery-2]").on("change",function(){



                $( "#vertical-radio-item-C-O-D" ).attr('class', 'vertical-radio__item');

                if($( "#delivery-inputs-delivery-to-department" ).is(":visible")){



                }else{

                    $( "#delivery-inputs-delivery-to-home" ).hide();
                    $( "#delivery-inputs-delivery-to-Kiev" ).hide();
                    $( "#delivery-inputs-pickup" ).hide();
                    $( "#delivery-inputs-delivery-to-department" ).show();

                }
            });
            $("input[type=radio][id=delivery-3]").on("change",function(){



                $( "#vertical-radio-item-C-O-D" ).attr('class', 'vertical-radio__item');

                if($( "#delivery-inputs-delivery-to-Kiev" ).is(":visible")){



                }else{

                    $( "#delivery-inputs-delivery-to-home" ).hide();
                    $( "#delivery-inputs-delivery-to-department" ).hide();
                    $( "#delivery-inputs-pickup" ).hide();
                    $( "#delivery-inputs-delivery-to-Kiev" ).show();

                }
            });

            $("input[type=radio][id=delivery-4]").on("change",function(){



                $( "#vertical-radio-item-C-O-D" ).attr('class', 'vertical-radio__item disabled');

                if($( "#delivery-inputs-pickup" ).is(":visible")){



                }else{

                    $( "#delivery-inputs-delivery-to-home" ).hide();
                    $( "#delivery-inputs-delivery-to-department" ).hide();
                    $( "#delivery-inputs-delivery-to-Kiev" ).hide();
                    $( "#delivery-inputs-pickup" ).show();

                }

            });
            $(this.selectors.formWrapper).on('submit', function (e) {
                e.preventDefault();


                $.fn.serializeObject = function () {
                    var o = {};
                    var a = this.serializeArray();
                    $.each(a, function () {
                        if (o[this.name]) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    return o;
                };


                var deliveryID=3;
                var unvalid_data = $('#orderData input:visible');

                unvalid_data=unvalid_data.serializeObject();


                if($("#person-1").is(':checked')){
                    unvalid_data["PERSON_TYPE"] = "1";

                }else if($("#person-2").is(':checked')){
                    unvalid_data["PERSON_TYPE"] = "2";
                }else{
                    unvalid_data["PERSON_TYPE"] = "ERROR! NOTHING IS CHECKED";
                }

                if($("#delivery-1").is(':checked')){
                    unvalid_data["DELIVERY_ID"] = "1";

                }else if($("#delivery-2").is(':checked')){
                    unvalid_data["DELIVERY_ID"] = "2";
                }else if($("#delivery-3").is(':checked')){
                    unvalid_data["DELIVERY_ID"] = "3";
                }else if($("#delivery-4").is(':checked')){
                    unvalid_data["DELIVERY_ID"] = "4";
                }else{
                    unvalid_data["DELIVERY_ID"] = "ERROR! NOTHING IS CHECKED";
                }


                if($("#u-14").is(':checked')){
                    unvalid_data["PAY_SYSTEM_ID"] = "1";

                }else if($("#u-15").is(':checked')){
                    unvalid_data["PAY_SYSTEM_ID"] = "2";
                }else if($("#imposed").is(':checked')){
                    unvalid_data["PAY_SYSTEM_ID"] = "3";
                }else{
                    unvalid_data["PAY_SYSTEM_ID"] = "ERROR! NOTHING IS CHECKED";
                }
                unvalid_data["sessid"]= $('input[name=sessid]').val();
                unvalid_data["SITE_ID"]= "s1";
                unvalid_data["location_type"] = "code";
                unvalid_data["BUYER_STORE"] = "1";
                unvalid_data["PERSON_TYPE_OLD"] = "1";
                unvalid_data["PROFILE_ID"] = "1";
                unvalid_data["ZIP_PROPERTY_CHANGED"] = "N";


                unvalid_data["ORDER_DESCRIPTION"] = "";
                unvalid_data["via_ajax"] = "Y";

                unvalid_data["action"] = 'saveOrderAjax';
                unvalid_data["sessid"]= $('input[name=sessid]').val();


                console.log(unvalid_data);
                $.ajax({
                    type:'POST',
                    url:'/personal/order.php',
                    data: unvalid_data,
                    success: function(response){
                        
                        if(response.order && response.order.REDIRECT_URL){
                            window.location.href=response.order.REDIRECT_URL;

                        }

                        //window.location.replace("/personal/order.php?ORDER_ID=29");

                    },
                    dataType: 'json'

                });

            });




        },
    };

})(window.Webvision);

$(function(){
    Webvision.Order.init();
});
/*




*/