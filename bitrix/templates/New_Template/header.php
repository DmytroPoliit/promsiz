<!DOCTYPE html>
<html lang="en-Us">
<head>
    <?$APPLICATION->ShowHead();?>

    <meta charset="UTF-8">
    <title><?$APPLICATION->ShowTitle();?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/libs.min.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/main.css">

</head>
<body>
<?$APPLICATION->ShowPanel();?>
<header class="site-header">
    <div class="site-header-top">
        <div class="container">
            <p class="site-header-top__prev"><?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/include/the_best_provider.php"
                    )
                );?></p>


            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "template_top_menu",
                Array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "COMPONENT_TEMPLATE" => "template_top_menu",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "top",
                    "USE_EXT" => "N"
                )
            );?>
        </div>
    </div>

    <div class="site-header-menu">
        <div class="site-header-menu__middle">
            <div class="container">
                <div class="main-logo">
                    <? if ($APPLICATION->GetCurPage(false) !== '/')
                    {?>
                    <a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/img/jpeg_files/logo.png" alt="site logo"></a>

                    <?}else{?>
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/jpeg_files/logo.png" alt="site logo">
                    <?}?>


                </div>
                <div class="main-connect">
                    <div class="dropdown">
                        <a href="tel:+38 (044) 390-40-90" class="dropdown-toggle" id="dropdownMenu1"
                           data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="true">
                            <span class="hidden-xs">+38 (044) <span class="bold-text">390-40-90</span></span>
                            <span class="icon-font icon-smartphone"></span>

                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="tel:+38(067)390-40-90"><?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/include/phone1.php"
                                        )
                                    );?></a>
                            </li>
                            <li><a href="tel:+38(067)390-40-90"> <?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/include/phone2.php"
                                        )
                                    );?></a>
                            </li>
                            <li><a href="tel:+38(067)390-40-90"><?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/include/phone3.php"
                                        )
                                    );?></a>
                            </li>
                            <li><a href="tel:+38(067)390-40-90"> <?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/include/phone4.php"
                                        )
                                    );?></a>
                            </li>
                        </ul>
                    </div>
                    <div class="main-connect__callback hidden-xs">
                        <a href="#" class="btn btn-md btn--gray" data-toggle="modal" data-target="#modal-callback-2">Заказать звонок</a>
                    </div>
                </div>

                <div class="main-nav">
                    <div class="main-nav-item main-nav-item__search">
                        <a href="#" class="search-trigger">
                            <span class="search-label">Поиск по сайту</span>
                            <span class="icon-font icon-search"></span>
                        </a>
                        <div class="site-search">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:search.form",
                                "search_template",
                                Array(
                                    "PAGE" => "#SITE_DIR#search/index.php",
                                    "USE_SUGGEST" => "N"
                                )
                            );?>
                            <table class="site-search__result">
                                <tbody>
                                <tr>
                                    <td>
                                        <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/img/jpeg_files/search-1.jpg" alt="icon"></a>
                                    </td>
                                    <td>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="goods-prop__name">
                                                    <a href="#">Перчатки антивибрационные Mechanix AV</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <s class="goods-price__old">715 грн</s>
                                                    <span class="goods-price__current">679 грн</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/img/jpeg_files/search-2.jpg" alt="icon"></a>
                                    </td>
                                    <td>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="goods-prop__name">
                                                    <a href="#">Перчатки антивибрационные Mechanix AV</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <s class="goods-price__old">715 грн</s>
                                                    <span class="goods-price__current">679 грн</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/img/jpeg_files/search-3.jpg" alt="icon"></a>
                                    </td>
                                    <td>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="goods-prop__name">
                                                    <a href="#">Перчатки антивибрационные Mechanix AV</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <s class="goods-price__old">715 грн</s>
                                                    <span class="goods-price__current">679 грн</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/img/jpeg_files/search-1.jpg" alt="icon"></a>
                                    </td>
                                    <td>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="goods-prop__name">
                                                    <a href="#">Перчатки антивибрационные Mechanix AV</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <s class="goods-price__old">715 грн</s>
                                                    <span class="goods-price__current">679 грн</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/img/jpeg_files/search-2.jpg" alt="icon"></a>
                                    </td>
                                    <td>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="goods-prop__name">
                                                    <a href="#">Перчатки антивибрационные Mechanix AV</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <s class="goods-price__old">715 грн</s>
                                                    <span class="goods-price__current">679 грн</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><a href="#" class="bold-text">Все результаты поиска</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="main-nav-item main-nav-item__sign">
                        <a href="#">
                            <span class="icon-font icon-user"></span> 
                        </a>
                    </div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:sale.basket.basket",
                        "header_basket",
                        Array(
                            "ACTION_VARIABLE" => "basketAction",
                            "AUTO_CALCULATION" => "Y",
                            "COLUMNS_LIST" => array(0=>"NAME",1=>"DISCOUNT",2=>"WEIGHT",3=>"DELETE",4=>"DELAY",5=>"TYPE",6=>"PRICE",7=>"QUANTITY",),
                            "CORRECT_RATIO" => "N",
                            "GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
                            "GIFTS_CONVERT_CURRENCY" => "N",
                            "GIFTS_HIDE_BLOCK_TITLE" => "N",
                            "GIFTS_HIDE_NOT_AVAILABLE" => "N",
                            "GIFTS_MESS_BTN_BUY" => "Выбрать",
                            "GIFTS_MESS_BTN_DETAIL" => "Подробнее",
                            "GIFTS_PAGE_ELEMENT_COUNT" => "4",
                            "GIFTS_PLACE" => "BOTTOM",
                            "GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
                            "GIFTS_PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
                            "GIFTS_SHOW_IMAGE" => "Y",
                            "GIFTS_SHOW_NAME" => "Y",
                            "GIFTS_SHOW_OLD_PRICE" => "N",
                            "GIFTS_TEXT_LABEL_GIFT" => "Подарок",
                            "HIDE_COUPON" => "N",
                            "OFFERS_PROPS" => "",
                            "PATH_TO_ORDER" => "/personal/order.php",
                            "PRICE_VAT_SHOW_VALUE" => "N",
                            "QUANTITY_FLOAT" => "N",
                            "SET_TITLE" => "Y",
                            "TEMPLATE_THEME" => "blue",
                            "USE_ENHANCED_ECOMMERCE" => "N",
                            "USE_GIFTS" => "Y",
                            "USE_PREPAYMENT" => "N"
                        )
                    );?><br>
                </div>
            </div>
        </div>

        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "menu_promsiz",
            Array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "left",
                "COMPONENT_TEMPLATE" => "menu_promsiz",
                "DELAY" => "N",
                "MAX_LEVEL" => "3",
                "MENU_CACHE_GET_VARS" => array(),
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "top",
                "USE_EXT" => "N"
            )
        );?>

</header>
<?$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "breadcrumps_promsiz",
    array(
        "COMPONENT_TEMPLATE" => "breadcrumps_promsiz",
        "START_FROM" => "0",
        "PATH" => "",
        "SITE_ID" => "s1"
    ),
    false
);?>
