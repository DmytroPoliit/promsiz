<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

?>


<?
//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn .= '<div class="breadcrumbs"><div class="container"> <ul class="breadcrumbs-list list-inline">';

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()


for($index = 0, $itemSize= count($arResult); $index < $itemSize; $index++ )
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);




	if($arResult[$index]["LINK"] <> "" )
	{
		$strReturn .= '	<li ><a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'</a></li>';
	}
	else
	{
		$strReturn .= '<li ><a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'</a></li>';
	}
}

$strReturn .= '</ul></div></div>';

return $strReturn;
