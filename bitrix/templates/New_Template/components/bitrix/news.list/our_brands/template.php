<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="main-brands">
    <div class="container">
        <div class="main-brands__title">
            <p class="block-title">Наши бренды:</p>
        </div>

        <ul class="main-brands__list list-inline">

<?foreach($arResult["ITEMS"] as $arItem):?>

            <li><a href="#"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="brand"></a></li>


<?endforeach;?>

        </ul>

        <div class="main-brands__link">
            <a href="/about_company/nashi-klienty/" class="btn btn-md btn--gray"><span>Смотреть все</span></a>
        </div>

    </div>
</div>




