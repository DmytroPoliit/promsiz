<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS'])) {?>
    <div class="top-slider">
        <?foreach ($arResult['ITEMS'] as $arItem){?>
            <div style="background-image: url('<?=$arItem['DETAIL_PICTURE']['SRC']?>')">
                <div class="container top-slider__item">
                    <p class="top-slider__item--title">
                        <?=$arItem['PROPERTIES']['NAME_HTML']['~VALUE']['TEXT']?>
                    </p>

                    <p class="top-slider__item--text hidden-xs">
                        <?=$arItem['PROPERTIES']['DESC_DESC']['~VALUE']['TEXT']?>
                    </p>
                    <p class="top-slider__item--text hidden-lg hidden-md hidden-sm">
                        <?=$arItem['PROPERTIES']['DESC_MOB']['~VALUE']['TEXT']?>
                    </p>
                    <a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" class="btn top-slider__item--link btn-accent--hover"><span class="hidden-xs">Узнать</span> подробнее</a>

                    <div class="subImage">
                        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="mans">
                    </div>
                </div>
            </div>
        <?}?>
    </div>
<?}?>



