<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>



<section class="container block-bottom-padding--md">


<h2 class="block-title margin-bottom-md text-center">Реализованные проекты</h2>

<div class="three-item-slider">
    <?foreach($arResult["ITEMS"] as $arItem):?>


        <div>
            <div class="project">
                <div class="project-image">
                    <a href="#">
                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="project">
                    </a>
                </div>
                <div class="project-link text-center">
                    <a href="#" class="bold-text"><?echo $arItem["NAME"]?></a>
                </div>
                <div class="project-view-more">
                    <a href="#" class="btn btn-md--gray-border btn-accent--hover">Подробнее</a>
                </div>
            </div>
        </div>

    <?endforeach;?>
</div>
</section>