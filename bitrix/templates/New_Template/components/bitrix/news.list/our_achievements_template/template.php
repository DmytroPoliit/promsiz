<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<section class="block-bottom-padding--md">
    <div class="container">
        <ul class="three-list list-inline">
            <?$source_photo="/img/d.jpg";?>
            <?foreach($arResult["ITEMS"] as $key => $arItem):?>

            <li class="three-list__item">
                <div class="reviews-list__top--wrapper">
                    <a href="#" class="reviews-list__item--image" data-toggle="modal" data-target="#rev-<?=$key?>">
                        <svg class="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="36" height="36" viewBox="0 0 36 36" style="">
                            <image id="Shape_135_copy" data-name="Shape 135 copy" width="36" height="36" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAMAAADW3miqAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABQVBMVEUAAAD///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8AAABuOdDmAAAAaXRSTlMABUuUyu331qhmFQ586vunKVHn/IwHAo7zolwuExAjSYjdzBul9XoNAVDgHcgfBo3XD7QKaeYgvNImRqqTSiHVR71XuF5N8DAx+Ya+DOyFOX8l69sIOjYnAx7Y/ayHwLrL8rLTCaO/apyfS9QVAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAALEgAACxIB0t1+/AAAAaVJREFUOMutlNdaAjEQhSMWBFHRxQIoShEXVEDFjtgABcVesPdy3v8FHNjdbHYJnzfO1ZmT/8skk8KYEW2O9o5OoMvZ7XIzefR4esGjr98rYwYGYQnFN2RHhkfqA6P+QHBsPDQxGa5nkaiNiZE5FZ82cjXhJCM5Y4Hq88zOiU4qTVZmXlwPGQuLthVkyewX9rVE89gZxjxEOcRsarl5v+oKsKoaPaT+xGVdWVOAdV07aO89fIRqcL0B+HWZM6UN2gR69XodQEAOLfYBeU3SmQblENsCtrk9xpURmrED7PKh8RbQHlDQVBcQagHRcouaygAT8jWpJWBfk93ApBw6AMr6rXIBYVUKVYBDXbqpGwkZdFQFskZC9ZwpydkdAyV+Xl46yHQzc0IVTs3UB2FeI87OgYtLMx+KEOVRLcwVMbgWnWiSnJ0b0ziqKPWWxmoidXtH1v1DsXGJhw8eq3rjrdT8hvZyM0/PL68NeXHy1kQxx7v4gEunHywlodT1T/03KH99N/ojo4jLb+8Wivv8F5BT9vhX6oGonz8gVqMfR/kLYrUfJfcLGQeTqMa/aowAAAAASUVORK5CYII="/>
                        </svg><?$source_photo=$arItem["PREVIEW_PICTURE"]["SRC"]?>
                        <img src="<?=$source_photo?>" alt="document">

                    </a>
                </div>
                <div class="reviews-list__bottom--wrapper">
                    <a href="#" class="reviews-list__item--title"><?echo $arItem["NAME"]?></a>
                </div>

                <div id="rev-<?=$key?>" class="modal fade image-modal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal"><i class="icon-sprite icon-close"></i></a>
                            </div>
                            <div class="modal-body">
                                <svg class="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="36" height="36" viewBox="0 0 36 36" style="">
                                    <image id="Shape_135_copy" data-name="Shape 135 copy" width="36" height="36" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAMAAADW3miqAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABQVBMVEUAAAD///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8AAABuOdDmAAAAaXRSTlMABUuUyu331qhmFQ586vunKVHn/IwHAo7zolwuExAjSYjdzBul9XoNAVDgHcgfBo3XD7QKaeYgvNImRqqTSiHVR71XuF5N8DAx+Ya+DOyFOX8l69sIOjYnAx7Y/ayHwLrL8rLTCaO/apyfS9QVAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAALEgAACxIB0t1+/AAAAaVJREFUOMutlNdaAjEQhSMWBFHRxQIoShEXVEDFjtgABcVesPdy3v8FHNjdbHYJnzfO1ZmT/8skk8KYEW2O9o5OoMvZ7XIzefR4esGjr98rYwYGYQnFN2RHhkfqA6P+QHBsPDQxGa5nkaiNiZE5FZ82cjXhJCM5Y4Hq88zOiU4qTVZmXlwPGQuLthVkyewX9rVE89gZxjxEOcRsarl5v+oKsKoaPaT+xGVdWVOAdV07aO89fIRqcL0B+HWZM6UN2gR69XodQEAOLfYBeU3SmQblENsCtrk9xpURmrED7PKh8RbQHlDQVBcQagHRcouaygAT8jWpJWBfk93ApBw6AMr6rXIBYVUKVYBDXbqpGwkZdFQFskZC9ZwpydkdAyV+Xl46yHQzc0IVTs3UB2FeI87OgYtLMx+KEOVRLcwVMbgWnWiSnJ0b0ziqKPWWxmoidXtH1v1DsXGJhw8eq3rjrdT8hvZyM0/PL68NeXHy1kQxx7v4gEunHywlodT1T/03KH99N/ojo4jLb+8Wivv8F5BT9vhX6oGonz8gVqMfR/kLYrUfJfcLGQeTqMa/aowAAAAASUVORK5CYII="/>
                                </svg>
                                <img src="<?=$source_photo?>" alt="document">
                            </div>
                            <div class="modal-footer">
                                <p class="reviews-modal-subtitle"><?echo $arItem["NAME"]?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <?endforeach;?>


        </ul>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <br /><?=$arResult["NAV_STRING"]?>
        <?endif;?>
    </div>

</section>