<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="block-with-back">
    <div class="container">
        <div class="block-with-back__title text-center">
            <h3 class="block-title">Новости</h3>

            <p class="block-subtitle text-center">Последние события нашей компании</p>
        </div>

        <div class="row">
            <?foreach($arResult["ITEMS"] as $arItem):?>

            <div class="col-md-6 col-sm-12">
                <div class="main-news-item">
                    <div class="main-news-item__image">
                        <a href="#">
                            <time><span class="bold-text">12</span>янв</time>
                            <img src=" <?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="news image">
                        </a>
                    </div>
                    <div class="main-news-item__prop">
                        <a href="#" class="main-news-item__prop--title"> <?echo $arItem['NAME']?></a>

                        <p class="main-news-item__prop--text"><?echo $arItem['PREVIEW_TEXT']?></p>
                        <a href="#" class="main-news-item__prop--link">Читать подробнее</a>
                    </div>
                </div>
            </div>

            <?endforeach;?>
        </div>

        <div class="block-with-back__link text-center">
            <a href="#" class="btn btn-md btn--gray"><span>Смотреть все</span></a>
        </div>
    </div>
</div>


