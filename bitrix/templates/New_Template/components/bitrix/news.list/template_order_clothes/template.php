<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="main-connect-item__order"
     style="background: url('<?=SITE_TEMPLATE_PATH?>/img/jpeg_files/connect-1.jpg') no-repeat center / cover;">
    <div class="left">
        <p class="block-title"> Заказать спецодежду </p>

        <p>Заказать спецодежду, спецобувь, оборудование или получить консультацию по всем товарам,
            представленным в нашем каталоге, так же можно связаться с нашим отделом продаж</p>
    </div>
    <div class="right">
        <a href="#" class="btn btn-md btn--accent">Заказать</a>
        <a href="tel:(044) 390-40-90" class="link-tel">(044) 390-40-90</a>
        <a href="mailto:info@promsiz.com.ua" class="link-mail">info@promsiz.com.ua</a>
    </div>
</div>


