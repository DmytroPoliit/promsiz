<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block-no-back">
    
    <div class="container">
        <div class="our-services">
            <div class="our-services__all">
                <h2 class="our-services__all--title">Наши услуги</h2>
                <a href="#" class="btn btn-with-arrow btn-md--white-border"><span>Смотреть все</span></a>
            </div>
            <ul class="our-services__list list-inline">
<?foreach($arResult["ITEMS"] as $arItem):?>
    <li class="our-services__list--itemTop">
        <a href="#">
            <img src="<?echo $arItem['PREVIEW_PICTURE']['SRC']?>" alt="foto">
            <span><?echo $arItem["NAME"]?></span>
        </a>
    </li>

<?endforeach;?>

            </ul>


        </div>
    </div>
</div>


