<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="block-bottom-padding--md">
    <div class="container">
        <ul class="three-list list-inline">

            <?foreach($arResult["ITEMS"] as $arItem):?>
            <li class="three-list__item ">
                <div class="project">
                    <div class="project-image">
                        <a href="#">
                            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="project">
                        </a>
                    </div>
                    <div class="project-link text-center">
                        <a href="#" class="bold-text"><?echo $arItem["NAME"]?></a>
                    </div>
                    <div class="project-view-more">
                        <a href="#" class="btn btn-md--gray-border btn-accent--hover">Подробнее</a>
                    </div>
                </div>
            </li>
            <?endforeach;?>


        </ul>
        


        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <br /><?=$arResult["NAV_STRING"]?>
        <?endif;?>

    </div>
</section>
