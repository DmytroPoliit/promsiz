<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>




    <div class="container">
        <div class="client-detail">
            <div class="float-left-block client-detail__image">
                <a href="#">
                    <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="image">
                </a>
            </div>
            <div class="client-detail__text">
                <p class="margin-bottom-md normal-text client-detail__text--title"><?=$arResult['DISPLAY_PROPERTIES']['ATT_ARTICLE_DETAIL']['VALUE']['TEXT']?></p>

                <ul class="not-num-list">
                    <li><span class="text-gray">Сайт партнера:</span> <a href="" class="link-accent"><?=$arResult['PROPERTIES']['ATT_PARTNER_SITE']['VALUE']?></a></li>
                    <li><span class="text-gray">Статус компании “ПромСИЗ”:</span> <span class="normal-text">официальный дистрибютор в Украине</span>
                    </li>
                </ul>
            </div>
        </div>



    <p class="margin-bottom-md"> <?=$arResult['PREVIEW_TEXT']?></p>

    <p class="margin-bottom-md"><?=$arResult['DETAIL_TEXT']?></p>
</div>


<div class="container padding-bottom-md">
    <h2 class="block-with-back__title text-center">Товары бренда</h2>
    <div class="four-item-slider no-arrows">

            <?
            if(CModule::IncludeModule("iblock")){
                $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*", "PREVIEW_PICTURE", "catalog_PRICE_1", "DETAIL_PAGE_URL");
                $arFilter = Array(
                    "ID"=>$amount_binds,
                    "IBLOCK_ID"=>9,
                    "ACTIVE_DATE"=>"Y",
                    "ACTIVE"=>"Y",
                    "PROPERTY_ATT_MANUFACTER_VALUE"=>$arResult['NAME']
                );

                $res = CIBlockElement::GetList(
                    Array(),
                    $arFilter,
                    false,
                    false,
                    $arSelect
                );



                while($ob = $res->GetNextElement())
                {



                    $arFields = $ob->GetFields();
                    $PREVIEW_PICTURE = CFile::GetPath($arFields["PREVIEW_PICTURE"]);
                    $NAME = ($arFields["NAME"]);
                    $DETAIL_PAGE_URL= ($arFields["DETAIL_PAGE_URL"]);
                    $arFields['PROPERIES'] = $ob->GetProperties();
                    $arFields['PRICE'] = CPrice::GetBasePrice($arFields['ID']);
                    $arFields['PRICE']['PRINT_VALUE'] = CurrencyFormat($arFields['PRICE']['PRICE'], $arFields['PRICE']['CURRENCY']);
                    $PRINT_VALUE=($arFields['PRICE']['PRINT_VALUE']);
                    ?>
        <div>
            <div class="goods">
                <div class="goods-image">
                    <a href="#">
                        <img src="<?=$PREVIEW_PICTURE?>" alt="goods">
                    </a>

                </div>
                <div class="goods-prop">
                    <div class="goods-prop__name">
                        <a href="<?=$DETAIL_PAGE_URL?>"><?=$NAME?></a>
                    </div>
                    <div class="goods-price">
                        <s class="goods-price__old">715 грн</s>
                        <span class="goods-price__current">679 грн</span>
                    </div>
                    <div class="goods-show-more">
                        <a href="<?=$DETAIL_PAGE_URL?>" class="btn">Подробнее</a>
                    </div>
                </div>
            </div>
        </div>


                    <?
                }


            }?>
    </div>

    <div class="block-with-back__link text-center">
        <a href="#" class="btn btn-md btn--gray"><span>Смотреть все</span></a>
    </div>

