<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="block-bottom-padding--md">
    <div class="container">
       
        <ul class="remedies-list">
            <?foreach($arResult["ITEMS"] as $arItem){?>

                <li class="remedies-item">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <span class="brands">

                        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="brand">

                    </span>

                    </a>
                </li>

            <?}?>
        </ul>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <br /><?=$arResult["NAV_STRING"]?>
        <?endif;?>
    </div>
</section>