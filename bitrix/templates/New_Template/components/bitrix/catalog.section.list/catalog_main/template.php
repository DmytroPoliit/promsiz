<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<ul class="remedies-list">

<?foreach ($arResult['SECTIONS'] as $arSection)
{?>
   
    <li class="remedies-item">
        <a href="<?=$arSection['SECTION_PAGE_URL']?>">
                    <span class="remedies-item__image">
                        <img src="<?=$arSection['PICTURE']['SRC']?>" alt="">


                    </span>
            <span><?=$arSection["NAME"]?></span>
        </a>
    </li>
<?}?>
</ul>





