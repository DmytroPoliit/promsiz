<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<section class="block-bottom-padding--md">
    <div class="container">
<p class="margin-bottom-md">
    Компания «ПромСИЗ» — национальный производитель, разработчик и поставщик средств индивидуальной защиты, одно
    из
    крупнейших предприятий в Украине по комплексному обслуживанию клиентов различных отраслей промышленности.
    Компания «ПромСИЗ» национальный производитель, разработчик и поставщик промышленных средств индивидуальной
    защиты, одно из крупнейших предприятий в Украине по комплексному обслуживанию клиентов различных отраслей
    промышленности.
</p>

<p class="margin-bottom-md">
    С 1998 года наша компания обеспечивает промышленные предприятия Украины и страны ближнего зарубежья
    средствами
    индивидуальной защиты, которые гарантируют безопасность рабочих при выполнении производственных задач и
    повышают
    производительность труда на предприятиях.Компания «ПромСИЗ» — национальный производитель, разработчик и
    поставщик средств индивидуальной защиты, одно из крупнейших предприятий в Украине.
</p>
<ul class="remedies-list">
    <?foreach ($arResult['SECTIONS'] as $arSection)
    {?>
        <li class="remedies-item">
            <a href="<?=$arSection['SECTION_PAGE_URL']?>">
                    <span class="remedies-item__image">
                        <img src="<?=$arSection['PICTURE']['SRC']?>" alt="">


                    </span>
                <span><?=$arSection["NAME"]?></span>
            </a>
        </li>
    <?}?>
</ul>

    </div>
</section>


