<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);
$this->addExternalCss('/bitrix/css/main/bootstrap.css');?>
<?if($_SESSION["params_row"] == 1){?>
<ul class="catalog catalog-col list-inline">
    <?foreach ($arResult['ITEMS'] as $arItem){?>
        <li class="catalog-col__item">


            <div class="goods">
                <div class="goods-image">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" tabindex="0">
                        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="goods">
                    </a>
                </div>
                <div class="goods-prop">
                    <div class="goods-prop__name">
                        <a href="#" tabindex="0"><?=$arItem['NAME']?></a>
                    </div>
                    <div class="goods-price">
                        <s class="goods-price__old">
                            <?
                            if($arItem['PRICES']['BASE']['PRINT_VALUE'] == $arItem['PRICES']['BASE']['PRINT_DISCOUNT_VALUE']){

                            }else{
                                echo $arItem['PRICES']['BASE']['PRINT_VALUE'];
                            }

                            ?>
                        </s>
                        <span class="goods-price__current"><?=$arItem['PRICES']['BASE']['PRINT_DISCOUNT_VALUE']?></span>
                    </div>

                    <div class="goods-show-more">
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="btn" tabindex="0">Подробнее</a>
                    </div>
                </div>
            </div>





        </li>
        <?if($arItem['ID']=="674"){?>
        <div style="color:red;font-size:1.5em;background-color:black;">
        <?
        echo "<pre>";
        print_r($arItem);
        echo "</pre>"; ?>
        </div><?}?>
    <?}?>
<?}else{?>
    <ul class="catalog catalog-row">

        <?foreach ($arResult['ITEMS'] as $arItem){?>





        <li class="catalog-row__item">
            <div class="goods">
                <div class="goods-image">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="goods">
                    </a>
                </div>
                <div class="goods-prop">
                    <div class="goods-prop__name">
                        <a href="#"><?=$arItem['NAME']?></a>
                    </div>

                    <div class="goods-manufacturer">
                        <span class="normal-text">Производитель:</span><a href="#"> <?=$arItem['PROPERTIES']['ATT_MANUFACTER']['VALUE']?></a>
                    </div>
                    <div class="goods-order-state">
                        <span class="normal-text">Статус: </span>
                        <?

                        if($arItem['PROPERTIES']['ATT_STATUS']['VALUE_XML_ID']== available){?>
                            <a href="#" class="status-success"><?echo $arItem['PROPERTIES']['ATT_STATUS']['VALUE']; ?></a><?
                        }else if($arItem['PROPERTIES']['ATT_STATUS']['VALUE_XML_ID']== waiting){?>
                            <a href="#" class="status-expected"><?echo $arItem['PROPERTIES']['ATT_STATUS']['VALUE']; ?></a><?
                        }else if($arItem['PROPERTIES']['ATT_STATUS']['VALUE_XML_ID']== order_only) {?>
                            <a href="#" class="status-order"><?echo $arItem['PROPERTIES']['ATT_STATUS']['VALUE'];?></a><?
                        }



                        ?>
                    </div>
                    <div class="goods-size">
                        <span class="goods-size__label normal-text">Размер:</span>
                        <ul class="goods-size__list list-inline">
                            <?foreach ($arItem['PROPERTIES']['ATT_SIZE']['VALUE_XML_ID'] as $counter1){
                            if($counter1==M){
                                ?><li><a href="#">(M)</a></li><?
                            }
                            if($counter1==L){
                                    ?><li><a href="#">(L)</a></li><?
                            }
                            if($counter1==XL){
                                    ?><li><a href="#">(XL)</a></li><?
                            }
                            if($counter1==XXL){
                                    ?><li><a href="#">(XXL)</a></li><?
                            }


                            }?>

                        </ul>
                    </div>
                    <div class="goods-color">
                        <span class="goods-color__label normal-text">Цвет:</span>
                        <ul class="goods-color__list list-inline">
                            <?foreach ($arItem['PROPERTIES']['ATT_COLOR']['VALUE_XML_ID'] as $counter2){
                                if($counter2==COLOR_WHITE){
                                    ?><li><a href="#" style="background-color: #dde4de"></a></li><?
                                }
                                if($counter2==COLOR_YELLOW){
                                    ?><li><a href="#" style="background-color: #dddcbe"></a></li><?
                                }
                                if($counter2==COLOR_GREEN){
                                    ?><li ><a href="#" style="background-color: #ceead3"></a></li><?
                                }
                                if($counter2==COLOR_RED){
                                    ?><li><a href="#" style="background-color: #efd8d1"></a></li><?
                                }


                            }?>




                        </ul>
                    </div>

                </div>

                <div class="goods-count">
                    <div class="goods-count__wrapper">
                        <div class="goods-price">
                            <s class="goods-price__old">

                            <?
                            if($arItem['PRICES']['BASE']['PRINT_VALUE'] == $arItem['PRICES']['BASE']['PRINT_DISCOUNT_VALUE']){

                            }else{
                                echo $arItem['PRICES']['BASE']['PRINT_VALUE'];
                            }

                            ?>




                            </s>
                            <span class="goods-price__current"><?=$arItem['PRICES']['BASE']['PRINT_DISCOUNT_VALUE']?></span>
                        </div>
                        <div class="goods-count-block">
                            <p class="goods-count-block__label">Количество:</p>

                            <form action="#">
                                <fieldset>
                                    <button type="button" class="btn dec">-</button>
                                    <input type="text" class="goods-amount-input" value="1 шт.">
                                    <button type="button" class="btn inc">+</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="goods-count__wrapper">
                        <div class="goods-order-btn">
                            <a href="<?=$arItem['ADD_URL']?>" class="btn btn-md btn--accent">Заказать товар</a>
                        </div>
                        <div class="goods-show-more">
                            <a href="#" class="btn">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
        </li>


    </ul>


<?}?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.pagenavigation",
        "page_navigation_main",
        Array()
    );?><br>
<?}?>
</ul>












