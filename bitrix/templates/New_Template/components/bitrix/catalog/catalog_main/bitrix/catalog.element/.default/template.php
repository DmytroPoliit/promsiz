<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);?>


<div class="product">
    <div class="container product-container-top">
        <div class="col-lg-5 col-md-12 no-padding">
            <div class="product-slider-block">
                <div class="product-slider-wrapper">
                    <div class="product-slider-top">
                        <div>
                            <div class="product-slider-top__item">
                                <img src="<?=$arResult['PREVIEW_PICTURE']['SRC']?>" alt="product">
                            </div>
                        </div>
                        <div>
                            <div class="product-slider-top__item">
                                <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="product">
                            </div>
                        </div>

                        <?foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $photo_counter){
                            $url_photo = CFile::GetPath($photo_counter);?>
                            <div>
                            <div class="product-slider-top__item">
                                <img src="<?=$url_photo?>" alt="product">
                            </div>
                        </div>
                        <?}?>



                    </div>

                    <div class="product-slider-bottom">
                        <div>
                            <div class="product-slider-bottom__item">
                                <img src="<?=$arResult['PREVIEW_PICTURE']['SRC']?>" alt="product">
                            </div>
                        </div>
                        <div>
                            <div class="product-slider-bottom__item">
                                <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="product">
                            </div>
                        </div>

                        <?foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $photo_counter){
                            $url_photo = CFile::GetPath($photo_counter);?>
                            <<div>
                                <div class="product-slider-bottom__item">
                                    <img src="<?=$url_photo?>" alt="product">
                                </div>
                            </div>
                        <?}?>


                    </div>
                </div>

            </div>
        </div>
        <div class="col-lg-7 col-md-12 no-padding product-underline-block">
            <div class="col-lg-7 col-md-6 col-sm-7 col-xs-12 no-padding">
                <div class="product-left-block">
                    <div class="product-label">
                        <a href="#">Артикул: 35623452</a>
                    </div>
                    <p class="paragraph-gray"><?=$arResult['PREVIEW_TEXT']?>
                    </p>

                    <div class="goods-size">
                        <span class="product-label">Размер:</span>
                        <ul class="goods-size__list list-inline">
                            <?foreach ($arResult['PROPERTIES']['ATT_SIZE']['VALUE_XML_ID'] as $counter1){
                                if($counter1==M){
                                    ?><li><a href="#">(M)</a></li><?
                                }
                                if($counter1==L){
                                    ?><li><a href="#">(L)</a></li><?
                                }
                                if($counter1==XL){
                                    ?><li><a href="#">(XL)</a></li><?
                                }
                                if($counter1==XXL){
                                    ?><li><a href="#">(XXL)</a></li><?
                                }


                            }?>

                        </ul>
                    </div>
                    <div class="goods-color">
                        <span class="product-label">Цвет:</span>
                        <ul class="goods-color__list list-inline">
                            <?foreach ($arResult['PROPERTIES']['ATT_COLOR']['VALUE_XML_ID'] as $counter2){
                                if($counter2==COLOR_WHITE){
                                    ?><li><a href="#" style="background-color: #dde4de"></a></li><?
                                }
                                if($counter2==COLOR_YELLOW){
                                    ?><li><a href="#" style="background-color: #dddcbe"></a></li><?
                                }
                                if($counter2==COLOR_GREEN){
                                    ?><li ><a href="#" style="background-color: #ceead3"></a></li><?
                                }
                                if($counter2==COLOR_RED){
                                    ?><li><a href="#" style="background-color: #efd8d1"></a></li><?
                                }


                            }?>

                        </ul>
                    </div>
                    <div class="product-gray-bg-block">
                        <div class="goods-price">
                            <span class="goods-price__current"><?=$arResult['PRICES']['BASE']['PRINT_VALUE'];?></span>
                            <s class="goods-price__old">
                                <?
                                if($arResult['PRICES']['BASE']['PRINT_VALUE'] == $arResult['PRICES']['BASE']['PRINT_DISCOUNT_VALUE']){

                                }else{

                                    echo $arResult['PRICES']['BASE']['PRINT_DISCOUNT_VALUE'];
                                }

                                ?>

                            </s>
                        </div>
                        <div class="goods-count-block">
                            <p class="goods-count-block__label">Количество:</p>

                            <form action="#">
                                <fieldset>
                                    <button type="button" class="btn dec">-</button>
                                    <input type="text" class="goods-amount-input" value="1 шт.">
                                    <button type="button" class="btn inc">+</button>
                                </fieldset>
                            </form>
                        </div>
                        <div class="goods-order-btn">
                            <a href="<?=$arResult['ADD_URL']?>" class="btn btn-md btn--accent">Добавить в корзину</a>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-5 col-xs-12 no-padding">
                <div class="product-right-block">
                    <p class="product-label">Защитные свойства:</p>
                    <ul class="product-protect-property list-inline">

                        <?foreach ($arResult['PROPERTIES']['ATT_DEFENCE_PROPERTIES']['VALUE_XML_ID'] as $application_defence_counter){?>
                            <?if($application_defence_counter==FIRE_DEFENCE){?>
                            <li>
                                <a href="#">
                                    <object type="image/svg+xml" data="<?=SITE_TEMPLATE_PATH?>/img/svg-image/prod-i-1.svg"></object>
                                </a>
                                <span class="hint">Защита от огня</span>
                            </li>
                            <?}else if($application_defence_counter==COLD_DEFENCE){?>
                            <li>
                                <a href="#">
                                    <object type="image/svg+xml" data="<?=SITE_TEMPLATE_PATH?>/img/svg-image/prod-i-2.svg"></object>
                                </a>
                                <span class="hint">Защита от холода</span>
                            </li>
                            <?}else if($application_defence_counter==NOICE_DEFENCE){?>
                            <li>
                                <a href="#">
                                    <object type="image/svg+xml" data="<?=SITE_TEMPLATE_PATH?>/img/svg-image/prod-i-3.svg"></object>
                                </a>
                                <span class="hint">Защита от шума</span>
                            </li>
                           <?}else if($application_defence_counter==VOBRATION_DEFENCE){?>
                            <li>
                                <a href="#">
                                    <object type="image/svg+xml" data="<?=SITE_TEMPLATE_PATH?>/img/svg-image/prod-i-4.svg"></object>
                                </a>
                                <span class="hint">Защита от вибрации</span>
                            </li>
                            <?}else?>
                        <?}?>






                    </ul>
                    <a href="#" class="product-help-link" data-toggle="modal" data-target="#modal-callback-1">
                        <span class="btn">Получить помощь </span>

                        <p>при выборе товара</p>
                    </a>

                    <p class="product-help-text">
                        Наши менеджеры проконсультируют Вас по всем вопросам. Оставьте заявку или же звоните по номеру:
                    </p>
                    <ul class="product-help-tel">
                        <li>
                            <a href="tel:+38 (044) 390-40-90" class="bold-text">+38 (044) 390-40-90</a>
                        </li>
                    </ul>
                    <ul class="product-other-property">
                        <li>
                            <a href="#">Таблица размеров</a>
                        </li>
                        <li>
                            <a href="#">Правила ухода</a>
                        </li>
                        <li>
                            <a href="#">Руководства по эксплуатации</a>
                        </li>
                    </ul>

                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div id="responsive-tab" class="product-tabs">
            <ul class="product-tabs__list list-inline">
                <li class="product-tabs__list--title"><a href="#tab-1">Назначение</a></li>
                <li class="product-tabs__list--title"><a href="#tab-2">Техническое описание</a></li>
                <li class="product-tabs__list--title"><a href="#tab-3">Область применения</a></li>
                <li class="product-tabs__list--title"><a href="#tab-4">Стандарты</a></li>
                <li class="product-tabs__list--title"><a href="#tab-5">Упаковка</a></li>
            </ul>
            <div class="tabs-content-wrapper">
                <div class="col-md-6 col-sm-12 no-padding left">
                    <div id="tab-1">
                        <div class="product-tabs__content">
                            <div class="product-tabs__content--title">
                                <span>Назначение</span>
                            </div>
                            <div class="product-tabs__content--text">
                                <p>
                                   <?=$arResult['PROPERTIES']['ATT_GOAL']['~VALUE']['TEXT'];?>

                                </p>
                            </div>
                        </div>
                    </div>
                    <div id="tab-2">
                        <div class="product-tabs__content">
                            <div class="product-tabs__content--title">
                                <span>Техническое описание</span>
                            </div>
                            <div class="product-tabs__content--text">
                                <p class="margin-bottom-sm"><span class="bold-text">Размер: </span>10 (XL)</p>
                                <p class="margin-bottom-sm"><span class="bold-text"> Основная защита:</span> <?=$arResult['PROPERTIES']['ATT_MAIN_DEFENCE']['VALUE']['TEXT'];?></p>
                                <p class="margin-bottom-sm"><span class="bold-text">Состав:</span>  <?=$arResult['PROPERTIES']['ATT_CONSISTANCY']['VALUE'];?></p>
                                <?=$arResult['PROPERTIES']['TECH_DESC']['~VALUE']['TEXT'];?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 no-padding right">
                    <div id="tab-3">
                        <div class="product-tabs__content">
                            <div class="product-tabs__content--title">
                                <span>Область применения</span>
                            </div>
                            <div class="product-tabs__content--text">
                                <ul class="not-num-list">
                                    <?foreach ($arResult['PROPERTIES']['APPLICATION_AREA']['VALUE'] as $application_area_counter){?>
                                        <li><span> <?=$application_area_counter ?> </span></li>
                                        
                                    <?}?>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="tab-4">
                        <div class="product-tabs__content">
                            <div class="product-tabs__content--title">
                                <span>Стандарты</span>
                            </div>
                            <div class="product-tabs__content--text">
                                <p><?=$arResult['PROPERTIES']['ATT_STANDARTS']['VALUE'];?></p>
                            </div>
                        </div>
                    </div>
                    <div id="tab-5">
                        <div class="product-tabs__content">
                            <div class="product-tabs__content--title">
                                <span>Упаковка</span>
                            </div>
                            <div class="product-tabs__content--text">
                                <?foreach ($arResult['PROPERTIES']['PACK']['VALUE'] as $application_pack_counter){?>
                                    <p> <?=$application_pack_counter ?> </p>
                                <?}?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="block-no-back">
    <div class="container">
        <div class="block-with-back__title text-center">
            <h2>Похожие товары</h2>
        </div>
        <div class="four-item-slider">



            <?foreach ($arResult['PROPERTIES']['ATT_BINDED']['VALUE'] as $amount_binds){?>
                <?
                if(CModule::IncludeModule("iblock")){
                    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*", "PREVIEW_PICTURE", "catalog_PRICE_1", "DETAIL_PAGE_URL");
                    $arFilter = Array(
                        "ID"=>$amount_binds,
                        "IBLOCK_ID"=>9,
                        "ACTIVE_DATE"=>"Y",
                        "ACTIVE"=>"Y"
                    );

                    $res = CIBlockElement::GetList(
                        Array(),
                        $arFilter,
                        false,
                        false,
                        $arSelect
                    );



                    while($ob = $res->GetNextElement())
                    {



                        $arFields = $ob->GetFields();
                        $arFields['PROPERIES'] = $ob->GetProperties();
                        $arFields['PRICE'] = CPrice::GetBasePrice($arFields['ID']);
                        $arFields['PRICE']['PRINT_VALUE'] = CurrencyFormat($arFields['PRICE']['PRICE'], $arFields['PRICE']['CURRENCY']);
                        $PREVIEW_PICTURE = CFile::GetPath($arFields["PREVIEW_PICTURE"]);
                        $NAME = ($arFields["NAME"]);
                        $DETAIL_PAGE_URL= ($arFields["DETAIL_PAGE_URL"]);
                        $PRINT_VALUE=($arFields['PRICE']['PRINT_VALUE']);

                    }

                }?>


            <div>

                <div class="goods">
                    <div class="goods-image">
                        <a href="<?=$DETAIL_PAGE_URL?>">
                            <img src="<?=$PREVIEW_PICTURE?>" alt="goods">
                        </a>
                    </div>
                    <div class="goods-prop">
                        <div class="goods-prop__name">
                            <a href="#"><?=$NAME?></a>
                        </div>
                        <div class="goods-price">

                            <span class="goods-price__current"><?=$PRINT_VALUE?></span>
                        </div>
                        <div class="goods-show-more">
                            <a href="<?=$DETAIL_PAGE_URL?>" class="btn">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
           <? }?>



        </div>
    </div>
</div>

<div class="block-no-back">
    <div class="container">
        <div class="block-with-back__title text-center">
            <h2>Сопутствующие товары</h2>
        </div>
        <div class="four-item-slider">
            <?foreach ($arResult['PROPERTIES']['ATT_ALSO_BUY']['VALUE'] as $amount_binds){?>
                <?
                if(CModule::IncludeModule("iblock")){
                    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*", "PREVIEW_PICTURE", "catalog_PRICE_1", "DETAIL_PAGE_URL");
                    $arFilter = Array(
                        "ID"=>$amount_binds,
                        "IBLOCK_ID"=>9,
                        "ACTIVE_DATE"=>"Y",
                        "ACTIVE"=>"Y"
                    );

                    $res = CIBlockElement::GetList(
                        Array(),
                        $arFilter,
                        false,
                        false,
                        $arSelect
                    );



                    while($ob = $res->GetNextElement())
                    {



                        $arFields = $ob->GetFields();
                        $PREVIEW_PICTURE = CFile::GetPath($arFields["PREVIEW_PICTURE"]);
                        $NAME = ($arFields["NAME"]);
                        $DETAIL_PAGE_URL= ($arFields["DETAIL_PAGE_URL"]);
                        $arFields['PROPERIES'] = $ob->GetProperties();
                        $arFields['PRICE'] = CPrice::GetBasePrice($arFields['ID']);
                        $arFields['PRICE']['PRINT_VALUE'] = CurrencyFormat($arFields['PRICE']['PRICE'], $arFields['PRICE']['CURRENCY']);
                        $PRINT_VALUE=($arFields['PRICE']['PRINT_VALUE']);

                    }


                }?>

                <div>
                    <div class="goods">
                        <div class="goods-image">
                            <a href="<?=$DETAIL_PAGE_URL?>">
                                <img src="<?=$PREVIEW_PICTURE?>" alt="goods">
                            </a>
                        </div>
                        <div class="goods-prop">
                            <div class="goods-prop__name">
                                <a href="#"><?=$NAME?></a>
                            </div>
                            <div class="goods-price">

                                <span class="goods-price__current"><?=$PRINT_VALUE?></span>
                            </div>
                            <div class="goods-show-more">
                                <a href="<?=$DETAIL_PAGE_URL?>" class="btn">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?}?>

        </div>
    </div>
</div>


