<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);
$this->addExternalCss('/bitrix/css/main/bootstrap.css');?>

<div class="four-item-slider">
    <div>
        <?foreach ($arResult['ITEMS'] as $arItem){?>
            <div class="goods">
                <div class="goods-image">
                    <a href="#">
                        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
                    </a>
                </div>
                <div class="goods-prop">
                    <div class="goods-prop__name">
                        <a href="#"><?=$arItem['NAME']?></a>
                    </div>
                    <div class="goods-price">
                        <s class="goods-price__old"><?=$arItem['PRICES']['BASE']['PRINT_VALUE']?></s>
                        <span class="goods-price__current">  <?=$arItem['PRICES']['BASE']['PRINT_DISCOUNT_DIFF']?>   </span>
                    </div>
                    <div class="goods-show-more">
                        <a href="#" class="btn">Подробнее</a>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
</div>
<div class="block-with-back__link text-center">
    <a href="#" class="btn btn-md btn--gray"><span>Смотреть все</span></a>
</div>


