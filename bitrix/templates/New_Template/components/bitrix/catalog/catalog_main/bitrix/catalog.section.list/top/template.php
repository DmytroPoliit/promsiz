<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<div class="right-block">
    <a class="catalog-title-mob bold-text">Рабочие перчатки</a>
    <ul class="five-item-list">
        <?foreach ($arResult['SECTIONS'] as $arSection)
        {?>
            <li>
            <a href="<?=$arSection['SECTION_PAGE_URL']?>">
                    <span class="five-item-list--photo">
                        <img src="<?=$arSection['PICTURE']['SRC']?>" alt="">

                    </span>
                <span><span class="five-item-list--title"><?=$arSection["NAME"]?></span></span>
            </a>
            </li>
        <?}?>
    </ul>

    <h2 class="right-block__title">Все товары “<?=$arResult['SECTION']["NAME"]?>”</h2>

    <div class="goods-sort-panel">
        <div class="goods-sort__select">
            <label for="sel-1">Сортировать:</label>
            <select name="sort" id="sel-1" class="selectpicker">
                <option value="1">по популярности</option>
                <option value="2">по возрастанию</option>
                <option value="3">по убыванию</option>
                <option value="4">some action</option>
                <option value="5">some action</option>
                <option value="6">some action</option>
            </select>
        </div>
        <div class="goods-sort__kind">
            <a href="?row=true" class="goods-sort__kind--col active">
                <span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
            </a>
            <a href="?row=false" class="goods-sort__kind--row">
                <span></span><span></span><span></span>
            </a>
        </div>
    </div>
    <div class="clearfix"></div>