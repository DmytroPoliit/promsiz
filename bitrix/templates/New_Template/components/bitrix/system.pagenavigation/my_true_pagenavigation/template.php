<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>



    <?if($arResult["bDescPageNumbering"] === true):?>



<font class="text">

    <?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
        <?if($arResult["bSavePage"]):?>

            |
            <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">&#171;</a>
            |
        <?else:?>

            |
            <?if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):?>
                <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">&#171;</a>
                |
            <?else:?>
                <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">&#171;</a>
                |
            <?endif?>
        <?endif?>
    <?else:?>
        <?=GetMessage("nav_prev")?>&nbsp;|
    <?endif?>

    <?while($arResult["nStartPage"] >= $arResult["nEndPage"]):?>
        <?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>

        <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
            <li class="pagination-item active"><?=$NavRecordGroupPrint?></li>
        <?elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):?>
            <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><li class="pagination-item"><?=$NavRecordGroupPrint?></li></a>
        <?else:?>
            <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?><li class="pagination-item active">PAGEN_<?=$arResult["NavNum"]?>= ><?=$arResult["nStartPage"]?>"><?=$NavRecordGroupPrint?></a>
        <?endif?>

        <?$arResult["nStartPage"]--?>
    <?endwhile?>

    |

    <?if ($arResult["NavPageNomer"] > 1):?>
        <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">&#187;</a>
        |

    <?else:?>
        &#187;
    <?endif?>

    <?else:?>

    </font>

<div class="pagination">






    <div class="pagination">
        <span class="pagination-label">Страницы:</span>
        <ul class="pagination-list list-inline">
    <?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>

        <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
            <li class="pagination-item active"><a href=""><?=$arResult["nStartPage"]?></a></li>
        <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
            <li class="pagination-item "> <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"> <li class="pagination-item "> <?=$arResult["nStartPage"]?></a></li>
        <?else:?>
            <li class="pagination-item"> <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"> <li class="pagination-item "><?=$arResult["nStartPage"]?></a></li>
        <?endif?>
        <li class="pagination-item"> <a href=""><?$arResult["nStartPage"]++?></a></li>
    <?endwhile?>
        </ul>




        <div class="pagination-ctrl">
    <?if ($arResult["NavPageNomer"] > 1):?>

        <?if($arResult["bSavePage"]):?>


            <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" class="pagination-ctrl--left">&#171;</a>

        <?else:?>


            <?if ($arResult["NavPageNomer"] > 2):?>
                <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" class="pagination-ctrl--left">&#171;</a>
            <?else:?>
                <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="pagination-ctrl--left" class="pagination-ctrl--left">&#171;</a>
            <?endif?>

        <?endif?>

    <?else:?>
        &#171;
    <?endif?>




    <?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
        <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"class="pagination-ctrl--right">&#187;</a>

    <?else:?>
        &#187;
    <?endif?>

    <?endif?>
        </div>




</div>