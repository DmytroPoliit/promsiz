<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>







<form action="<?=$arResult["FORM_ACTION"]?>">
    <table border="0" cellspacing="10" cellpadding="2" align="center">
    <tr>
        <td align="center">
            <fieldset>
                <input type="text" name="q" value="" class="site-search__input" maxlength="125" size="125" height="42px" hautofocus/>
                <button type="reset" class="btn site-search__btn-reset"><i class="icon-sprite icon-close"></i></button>
                <button type="submit" class="btn site-search__btn-submit"><i class="icon-font icon-search"></i></button>
            </fieldset>
        </td>



        <td align="right">
            <input name="s" type="submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" class="btn btn--gray site-search__submit"/>

        </td>
    </tr>
    </table>
</form>