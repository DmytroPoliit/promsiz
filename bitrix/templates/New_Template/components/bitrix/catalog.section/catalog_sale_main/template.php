<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);
$this->addExternalCss('/bitrix/css/main/bootstrap.css');?>

<ul class="catalog catalog-col list-inline">

    <?foreach ($arResult['ITEMS'] as $arItem){?>
    <li class="catalog-col__item">
        <div class="goods">
                    <div class="goods-image">
                        <a href="#">
                            <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
                        </a>
                    </div>

                    <div class="goods-prop">
                        <div class="goods-prop__name">
                            <a href="#"><?=$arItem['NAME']?></a>
                        </div>
                        <div class="goods-price">
                            <s class="goods-price__old"><?=$arItem['PRICES']['BASE']['PRINT_VALUE']?></s>
                            <span class="goods-price__current">  <?=$arItem['PRICES']['BASE']['PRINT_DISCOUNT_VALUE']?>   </span>
                        </div>

                        <div class="goods-show-more">
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="btn">Подробнее</a>
                        </div>
                    </div>
        </div>
    </li>
    <?}?>
</ul>
<div class="block-with-back__link text-center">
    <a href="/catalog/" class="btn btn-md btn--gray"><span>Смотреть все</span></a>
</div>