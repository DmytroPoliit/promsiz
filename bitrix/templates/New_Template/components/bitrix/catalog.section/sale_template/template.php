<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);
?>
<ul class="catalog catalog-col list-inline">
    <?foreach ($arResult['ITEMS'] as $arItem){?>
        <?
        if($arItem['PRICES']['BASE']['VALUE']>$arItem['PRICES']['BASE']['DISCOUNT_VALUE']){?>


        <li class="catalog-col__item">


            <div class="goods">
                <div class="goods-image">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" tabindex="0">
                        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="goods">
                    </a>
                </div>
                <div class="goods-prop">
                    <div class="goods-prop__name">
                        <a href="#" tabindex="0"><?=$arItem['NAME']?></a>
                    </div>
                    <div class="goods-price">
                        <s class="goods-price__old">
                            <?
                            if($arItem['PRICES']['BASE']['PRINT_VALUE'] == $arItem['PRICES']['BASE']['PRINT_DISCOUNT_VALUE']){

                            }else{
                                echo $arItem['PRICES']['BASE']['PRINT_VALUE'];
                            }

                            ?>
                        </s>
                        <span class="goods-price__current"><?=$arItem['PRICES']['BASE']['PRINT_DISCOUNT_VALUE']?></span>
                    </div>

                    <div class="goods-show-more">
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="btn" tabindex="0">Подробнее</a>
                    </div>
                </div>
            </div>





        </li>
    <?}?>
<?}?>