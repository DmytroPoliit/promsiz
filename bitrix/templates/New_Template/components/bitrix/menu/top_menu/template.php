<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

    <div class="main-drop-mnu">
        <a href="#" id="trigger-menu">
                        <span class="burger-menu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
            <span class="for-tablet">Меню</span>
            <span class="for-desktop">Каталог товаров</span>
        </a>
        <div class="main-drop-mnu__item">
            <div class="main-drop-mnu__item--title">
                <span>Каталог товаров</span>
            </div>
            <ul class="main-drop-mnu__item--list">
                <li class="has-child">
                    <div class="has-child__wrapper">
                        <a href="https://www.google.com.ua">Спецодежда</a>
                    </div>
                    <ul class="level-two">
                        <li class="tablet-navigate">
                            <div class="tablet-navigate__back">
                                <a href="#" class="btn"><span>Назад</span></a>
                            </div>
                            <div class="tablet-navigate__title">
                                <a href="#">Спецодежда</a>
                            </div>
                        </li>
                        <li class="level-two__item has-child">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-1.png" alt="menu">
                                <a href="https://www.google.com.ua">Пояса страховочные</a>
                            </div>
                            <ul class="level-two__item--list">
                                <li class="tablet-navigate">
                                    <div class="tablet-navigate__back">
                                        <a href="#" class="btn"><span>Назад</span></a>
                                    </div>
                                    <div class="tablet-navigate__title">
                                        <a href="#">Пояса страховочные</a>
                                    </div>
                                </li>
                                <li><a href="#">Пояса лямочные</a></li>
                                <li><a href="#">Пояса безлямочные (для удержания)</a></li>
                            </ul>
                        </li>
                        <li class="level-two__item has-child">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-2.png" alt="menu">
                                <a href="#">Стропы страховочные</a>
                            </div>
                            <ul class="level-two__item--list">
                                <li class="tablet-navigate">
                                    <div class="tablet-navigate__back">
                                        <a href="#" class="btn"><span>Назад</span></a>
                                    </div>
                                    <div class="tablet-navigate__title">
                                        <a href="#">Стропы страховочные</a>
                                    </div>
                                </li>
                                <li><a href="#">С амортизатором</a></li>
                                <li><a href="#">Без амортизатора</a></li>
                                <li><a href="#">Для позиционирования</a></li>
                            </ul>
                        </li>
                        <li class="level-two__item has-child">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-3.png" alt="menu">
                                <a href="#">Анкерные устройства</a>
                            </div>
                            <ul class="level-two__item--list">
                                <li class="tablet-navigate">
                                    <div class="tablet-navigate__back">
                                        <a href="#" class="btn"><span>Назад</span></a>
                                    </div>
                                    <div class="tablet-navigate__title">
                                        <a href="#">Анкерные устройства</a>
                                    </div>
                                </li>
                                <li><a href="#">Анкерные крепления</a></li>
                                <li><a href="#">Мобильные анкерные линии</a></li>
                            </ul>
                        </li>
                        <li class="level-two__item has-child">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-4.png" alt="menu">
                                <a href="#">Страховочные устройства втягивающего типа</a>
                            </div>
                            <ul class="level-two__item--list">
                                <li class="tablet-navigate">
                                    <div class="tablet-navigate__back">
                                        <a href="#" class="btn"><span>Назад</span></a>
                                    </div>
                                    <div class="tablet-navigate__title">
                                        <a href="#">Страховочные устройства втягивающего типа</a>
                                    </div>
                                </li>
                                <li><a href="#">Тросовые</a></li>
                                <li><a href="#">Ленточные</a></li>
                            </ul>
                        </li>
                        <li class="level-two__item has-child">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-5.png" alt="menu">
                                <a href="#">Стационарные системы страховки</a>
                            </div>
                            <ul class="level-two__item--list">
                                <li class="tablet-navigate">
                                    <div class="tablet-navigate__back">
                                        <a href="#" class="btn"><span>Назад</span></a>
                                    </div>
                                    <div class="tablet-navigate__title">
                                        <a href="#">Стационарные системы страховки</a>
                                    </div>
                                </li>
                                <li><a href="#">Горизонтальные стационарные системы страховки</a></li>
                                <li><a href="#">Вертикальные стационарные системы страховки</a></li>
                            </ul>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-6.png" alt="menu">
                                <a href="#">Оборудование для работ в ограниченном пространстве</a>
                            </div>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-7.png" alt="menu">
                                <a href="#">Устройства для спасения и эвакуации</a>
                            </div>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-8.png" alt="menu">
                                <a href="3">Снаряжение для промышленного альпинизма</a>
                            </div>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-9.png" alt="menu">
                                <a href="#">Когти, лазы и комплектующие</a>
                            </div>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-10.png" alt="menu">
                                <a href="#">Лестничные конструкции</a>
                            </div>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-11.png" alt="menu">
                                <a href="#">Веревки и шнуры</a>
                            </div>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-12.png" alt="menu">
                                <a href="#">Аксессуары</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li><a href="#">Спецобувь</a></li>
                <li><a href="#">Рабочие перчатки</a></li>
                <li><a href="#">Средства защиты глаз и лица</a></li>
                <li><a href="#">Средства защиты глаз и лица</a></li>
                <li><a href="#">Средства защиты органов слуха</a></li>
                <li><a href="#">Средства защиты органов дыхания</a></li>
                <li><a href="#">ДСИЗ</a></li>
                <li><a href="#">Защита от электрического тока</a></li>
                <li><a href="#">Защитные системы блокировки LOTO</a></li>
                <li class="has-child">
                    <div class="has-child__wrapper">
                        <a href="https://www.google.com.ua">Средства защиты от падения с высоты Средства защиты от падения с высоты Средства защиты от падения с высоты Средства защиты от падения с высоты</a>
                    </div>
                    <ul class="level-two">
                        <li class="tablet-navigate">
                            <div class="tablet-navigate__back">
                                <a href="#" class="btn"><span>Назад</span></a>
                            </div>
                            <div class="tablet-navigate__title">
                                <a href="#">Средства защиты от падения с высоты Средства защиты от падения с высоты Средства защиты от падения с высоты Средства защиты от падения с высоты</a>
                            </div>
                        </li>
                        <li class="level-two__item has-child">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-1.png" alt="menu">
                                <a href="https://www.google.com.ua"> Пояса страховочные Пояса страховочные Пояса страховочные Пояса страховочные Пояса страховочные Пояса страховочные Пояса страховочные</a>
                            </div>
                            <ul class="level-two__item--list">
                                <li class="tablet-navigate">
                                    <div class="tablet-navigate__back">
                                        <a href="#" class="btn"><span>Назад</span></a>
                                    </div>
                                    <div class="tablet-navigate__title">
                                        <a href="#">Пояса страховочные Пояса страховочные Пояса страховочные Пояса страховочные Пояса страховочные Пояса страховочные Пояса страховочные</a>
                                    </div>
                                </li>
                                <li><a href="#">Пояса лямочные</a></li>
                                <li><a href="#">Пояса безлямочные (для удержания)</a></li>
                            </ul>
                        </li>
                        <li class="level-two__item has-child">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-2.png" alt="menu">
                                <a href="#">Стропы страховочные</a>
                            </div>
                            <ul class="level-two__item--list">
                                <li class="tablet-navigate">
                                    <div class="tablet-navigate__back">
                                        <a href="#" class="btn"><span>Назад</span></a>
                                    </div>
                                    <div class="tablet-navigate__title">
                                        <a href="#">Стропы страховочные</a>
                                    </div>
                                </li>
                                <li><a href="#">С амортизатором</a></li>
                                <li><a href="#">Без амортизатора</a></li>
                                <li><a href="#">Для позиционирования</a></li>
                            </ul>
                        </li>
                        <li class="level-two__item has-child">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-3.png" alt="menu">
                                <a href="#">Анкерные устройства</a>
                            </div>
                            <ul class="level-two__item--list">
                                <li class="tablet-navigate">
                                    <div class="tablet-navigate__back">
                                        <a href="#" class="btn"><span>Назад</span></a>
                                    </div>
                                    <div class="tablet-navigate__title">
                                        <a href="#">Анкерные устройства</a>
                                    </div>
                                </li>
                                <li><a href="#">Анкерные крепления</a></li>
                                <li><a href="#">Мобильные анкерные линии</a></li>
                            </ul>
                        </li>
                        <li class="level-two__item has-child">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-4.png" alt="menu">
                                <a href="#">Страховочные устройства втягивающего типа</a>
                            </div>
                            <ul class="level-two__item--list">
                                <li class="tablet-navigate">
                                    <div class="tablet-navigate__back">
                                        <a href="#" class="btn"><span>Назад</span></a>
                                    </div>
                                    <div class="tablet-navigate__title">
                                        <a href="#">Страховочные устройства втягивающего типа</a>
                                    </div>
                                </li>
                                <li><a href="#">Тросовые</a></li>
                                <li><a href="#">Ленточные</a></li>
                            </ul>
                        </li>
                        <li class="level-two__item has-child">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-5.png" alt="menu">
                                <a href="#">Стационарные системы страховки</a>
                            </div>
                            <ul class="level-two__item--list">
                                <li class="tablet-navigate">
                                    <div class="tablet-navigate__back">
                                        <a href="#" class="btn"><span>Назад</span></a>
                                    </div>
                                    <div class="tablet-navigate__title">
                                        <a href="#">Стационарные системы страховки</a>
                                    </div>
                                </li>
                                <li><a href="#">Горизонтальные стационарные системы страховки</a></li>
                                <li><a href="#">Вертикальные стационарные системы страховки</a></li>
                            </ul>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-6.png" alt="menu">
                                <a href="#">Оборудование для работ в ограниченном пространстве</a>
                            </div>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-7.png" alt="menu">
                                <a href="#">Устройства для спасения и эвакуации</a>
                            </div>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-8.png" alt="menu">
                                <a href="3">Снаряжение для промышленного альпинизма</a>
                            </div>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-9.png" alt="menu">
                                <a href="#">Когти, лазы и комплектующие</a>
                            </div>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-10.png" alt="menu">
                                <a href="#">Лестничные конструкции</a>
                            </div>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-11.png" alt="menu">
                                <a href="#">Веревки и шнуры</a>
                            </div>
                        </li>
                        <li class="level-two__item">
                            <div class="level-two__item--title">
                                <img src="img/jpeg_files/m-12.png" alt="menu">
                                <a href="#">Аксессуары</a>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
            <ul class="main-drop-mnu__item--tabletSubMenu">
                <li><a href="#">О компании</a></li>
                <li><a href="#">Услуги</a></li>
                <li><a href="#">Клиентам</a></li>
                <li><a href="#">Информация</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
            <ul class="main-drop-mnu__item--tabletSubMenu">
                <li><a href="#">О нас</a></li>
                <li><a href="#">Как заказать</a></li>
                <li><a href="#">Оплата и доставка</a></li>
                <li><a href="#">Партнерство</a></li>
                <li><a href="#">Карьера</a></li>
                <li><a href="#">Связаться с нами</a></li>
            </ul>
        </div>
    </div>

<?if (!empty($arResult)):?>
<ul class="left-menu">

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<li><a href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>

</ul>
<?endif?>