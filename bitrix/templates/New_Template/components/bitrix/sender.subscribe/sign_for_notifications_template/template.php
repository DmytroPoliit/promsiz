<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="main-connect-item__order-right" style="background: url('<?=SITE_TEMPLATE_PATH?>/img/jpeg_files/connect-3.jpg') no-repeat center / cover;" >
                        <p class="block-title">Подпишитесь на рассылку новостей и акций</p>
                        <form action="#">
                            <input type="email" placeholder="Ваш e-mail" class="main-connect-item__input">
                            <button type="submit" class="btn btn--accent"><span class="icon-font icon-send"></span></button>
                        </form>
                    </div>