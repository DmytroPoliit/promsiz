<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
    foreach($arResult["ERROR_MESSAGE"] as $v)
        ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
    ?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>


<div class="mfeedback">

    <div class="col-md-5 col-sm-6 col-xs-12 no-padding">
        <div class="contacts-form">
            <div class="authorization-form text-center">
                <h2 class="authorization-form__title">Есть вопросы?</h2>

                <form action="<?=POST_FORM_ACTION_URI?>" method="POST" class="authorization-form__item">
                    <?=bitrix_sessid_post()?>
                    <fieldset>
                        <input type="text" id="f-1" class="modal-input" placeholder="Введите ваше имя" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>">
                        <label for="f-1"></label>
                        <?=GetMessage("MFT_NAME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
                        <div class="mf-name">
                            <div class="mf-text">



                        <input type="text" id="f-2" class="modal-input" placeholder="<?=GetMessage("MFT_EMAIL")?>" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>">
                        <label for="f-2"><mark class="mark-red"></mark> </label>
                        <?=GetMessage("MFT_EMAIL")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>

                        <textarea id="f-3" class="modal-input" placeholder="Введите текст вопроса" name="MESSAGE"></textarea>
                        <label for="f-3"><mark class="mark-red"></mark> </label>
                        <?=GetMessage("MFT_MESSAGE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
                    </fieldset>

                    <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
                    <button class="btn btn-md btn--accent" type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</div>






